# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.0.1] - [2023-03-29]


- see  #24727 upgrade version just for removing the gcube-staging dependencies 

## [v1.2.0] - [2021-06-08]

- Feature #21584 added support for /ServiceEndpoint/{category} REST call


## [v1.1.0-SNAPSHOT] - [2016-10-03]

- porting to auth v.2


## [v1.0.0] - [2015-07-01]

- First commit
